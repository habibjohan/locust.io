from locust import HttpUser, constant, task

class MyUsers(HttpUser):

    host = "https://comfortworks:C00lbananas@develop.comfort-works.com"
    wait_time = constant(1)

    @task(1)
    def homepage(self):
        self.client.get("/en/")
        self.client.get("/de-de/")
        self.client.get("/fr-fr/")
        self.client.get("/ja/")
        self.client.get("/es-es/")
        self.client.get("/it-it/")
        self.client.get("/nl-nl/")

    @task(2)
    def product_page(self):
        self.client.get("/en/angby/angby-armchair-slipcover-215/?F220-03")
    
    @task(1)
    def fabric_samples(self):
        self.client.get('/en/fabric-samples/fabric-samples-24/')